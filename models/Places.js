var mongoose = require('mongoose');

var PlaceSchema = new mongoose.Schema({
   name: String,
   images: Array,
   description: String,
   season: String,
   season_des: String,
   duration: String,
   duration_des: String,
   googlemap: String,
   tag: String,
   location: String,
   nearby: Array
});

mongoose.model('Place', PlaceSchema);
