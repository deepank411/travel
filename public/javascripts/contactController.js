app.controller('contactController', ['$scope', 'contactService',
   function($scope, contactService){

      $scope.contacts = contactService.contacts;
      $scope.addContact = function(){
         if(!$scope.name){
            return;
         }
         contactService.create({
            name: $scope.name,
            email: $scope.email,
            message: $scope.message
         });
         $scope.name = '';
         $scope.email = '';
         $scope.message = '';
      };
   }
]);
