var uniqueItems = function (data, key) {
    var result = [];
    for (var i = 0; i < data.length; i++) {
        var value = data[i][key];
        if (result.indexOf(value) == -1) {
            result.push(value);
        }
    }
    return result;
};

app.controller('listController',['$scope', 'placeService', 'filterFilter',
function($scope, placeService, filterFilter) {
   $scope.useSeasons = {};
   $scope.useDurations = {};
   $scope.useTags = {};
   $scope.useLocations = {};

   $scope.places = placeService.places;
   console.log(placeService.places);

   // Watch the pants that are selected
   $scope.$watch(function () {
      return {
         places: $scope.places,
         useSeasons: $scope.useSeasons,
         useDurations: $scope.useDurations,
         useTags: $scope.useTags,
         useLocations: $scope.useLocations
      }
   }, function (value) {
      var selected;

      // $scope.count = function (prop, value) {
      //    return function (el) {
      //       return el[prop] == value;
      //    };
      // };

      //season
      $scope.seasonsGroup = uniqueItems($scope.places, 'season');
      var filterAfterSeasons = [];
      selected = false;
      for (var j in $scope.places) {
         var p = $scope.places[j];
         for (var i in $scope.useSeasons) {
            if ($scope.useSeasons[i]) {
               selected = true;
               if (i == p.season) {
                  filterAfterSeasons.push(p);
                  break;
               }
            }
         }
      }
      if (!selected) {
         filterAfterSeasons = $scope.places;
      }

      //duration
      $scope.durationsGroup = uniqueItems($scope.places, 'duration');
      var filterAfterDurations = [];
      selected = false;
      for (var j in filterAfterSeasons) {
         var p = filterAfterSeasons[j];
         for (var i in $scope.useDurations) {
            if ($scope.useDurations[i]) {
               selected = true;
               if (i == p.duration) {
                  filterAfterDurations.push(p);
                  break;
               }
            }
         }
      }
      if (!selected) {
         filterAfterDurations = filterAfterSeasons;
      }

      //tag
      $scope.tagsGroup = uniqueItems($scope.places, 'tag');
      var filterAfterTags = [];
      selected = false;
      for (var j in filterAfterDurations) {
         var p = filterAfterDurations[j];
         for (var i in $scope.useTags) {
            if ($scope.useTags[i]) {
               selected = true;
               if (i == p.tag) {
                  filterAfterTags.push(p);
                  break;
               }
            }
         }
      }
      if (!selected) {
         filterAfterTags = filterAfterDurations;
      }

      //location
      $scope.locationsGroup = uniqueItems($scope.places, 'location');
      var filterAfterLocations = [];
      selected = false;
      for (var j in filterAfterTags) {
         var p = filterAfterTags[j];
         for (var i in $scope.useLocations) {
            if ($scope.useLocations[i]) {
               selected = true;
               if (i == p.location) {
                  filterAfterLocations.push(p);
                  break;
               }
            }
         }
      }
      if (!selected) {
         filterAfterLocations = filterAfterTags;
      }

      $scope.filteredPlaces = filterAfterLocations;
   }, true);


   $scope.$watch('filtered', function (newValue) {
      if (angular.isArray(newValue)) {
         console.log(newValue.length);
      }
   }, true);
}]);

app.filter('count', function() {
   return function(collection, key) {
      var out = "test";
      for (var i = 0; i < collection.length; i++) {
         //console.log(collection[i].pants);
         //var out = myApp.filter('filter')(collection[i].pants, "42", true);
      }
      return out;
   }
});


app.filter('groupBy', function () {
   return function (collection, key) {
      if (collection === null) return;
      return uniqueItems(collection, key);
   };
});
