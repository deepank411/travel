app.controller('locationController', ['$scope', 'placeService', 'place',
   function($scope,placeService, place) {
      $scope.place = place;
      $scope.mainImageUrl = place.images[0];
      $scope.setImage = function(imageUrl) {
         $scope.mainImageUrl = imageUrl;
      };
   }
]);
