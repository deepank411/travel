app.service('placeService', function ($http) {
  var _places = [];
  this.places = _places;

  this.getAll = function() {
    return $http.get('/places').success(function(data) {
      angular.copy(data, _places);
      console.log(data);
    });
  };

  this.get = function(id) {
    return $http.get('/places/' + id).then(function(res){
      return res.data;
    });
  }
});

app.service('contactService', function($http){
   var _contacts = [];
   this.contacts = _contacts;

   this.create = function(contact) {
      return $http.post('/contacts', contact)
         .success(function(data){
            _contacts.push(data);
            Materialize.toast('Thank you for your response', 4000);
            // console.log("form submitted");
         })
         .error(function(){
            Materialize.toast('Sorry, response was not submitted', 4000);
         });
   };
});
